import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
// Grid
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";

import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";

import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import Button from "components/CustomButtons/Button.jsx";

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import Datetime from "react-datetime";

import CustomInput from "components/CustomInput/CustomInput.jsx";
import DateRange from "@material-ui/icons/DateRange";
import AccessTime from "@material-ui/icons/AccessTime";
import Home from "@material-ui/icons/Home";

import InputAdornment from "@material-ui/core/InputAdornment";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import servicePageStyle from "assets/jss/material-kit-react/views/servicePage.jsx";

var arr = [];
class ServicePage extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      showCategory : true,
      showBrands : false,
      showServices : false,
      showPickUpDelivery:false,
      showCheckoutPage:false,

      openLoginModel: false,
      categoryList: [],
      selectedCategory : '',
      brandList: [],
      selectedBrand: '',
      servicesList: [],
      serviceIds: [],

      pickUpDeliveryList: [],
      pickupDeliveryServiceID:'',
      pickupDate: '',
      pickupTime: '',
      pickupAddress: '',
      deliveryDate: '',
      deliveryTime: '',
      deliveryAddress:'',
    };
  }

  componentDidMount(){
    this.getCategoryList();
    // this.getCategoryList();
    this.getBrandList();
    
    console.log(this.state.openLoginModel);
  }

  backToBookNewService(){
    console.log('back to new booking');
    this.setState({
      selectedCategory : '',
      selectedBrand: '',
      serviceIds: [],
      pickupDeliveryServiceID:'',
      pickupDate: '',
      pickupTime: '',
      pickupAddress: '',
      deliveryDate: '',
      deliveryTime: '',
      deliveryAddress:'',
      showBrands : false,
      showServices : false,
      showPickUpDelivery:false,
      showCategory : true,
    })
    arr = [];
  }

  getBrands(item){
    console.log('brands clicked');
    this.setState({
      selectedCategory : item,
      showCategory : false,
      showBrands : true,
    });
    this.getServicesList();
    this.getPickUpDeliveryList();
  }

  getServices(item){
    console.log(item);
    this.setState({
      selectedBrand : item,
      showBrands : false,
      showServices : true,
    })
  }

  selectService(event){
  var flag = false;
   if(arr.length < 0){
     arr.push(event.target.value);
   }else{
     for(var i=0; i< arr.length; i++){
       if(arr[i] === event.target.value){
         flag = true;
         if(flag){
          arr.splice(i,1);
          break;
         }
       }
     }
     if(!flag){
      arr.push(event.target.value);
     }
   }
   this.setState({ serviceIds : arr});
  }

  goBackToBrands(){
    this.setState({
      selectedBrand : '',
      showServices : false,
      showBrands : true,
      serviceIds: [],
    });
    arr = [];
  }

  goBackToServices(){
    this.setState({
      showPickUpDelivery:false,
      showServices:true,
      serviceIds: [],
      pickupDate: '',
      pickupTime: '',
      pickupAddress: '',
      deliveryDate: '',
      deliveryTime: '',
      deliveryAddress:'',
      orderComments:''
    });
    arr = [];
  }

  handlePickupDeliveryService = event => {
    this.setState({ pickupDeliveryServiceID: event.target.value });
  };

  convertToDate(val){
    var d = val.getDate();
    var m = val.getMonth()+1;
    var y = val.getFullYear();
    return (y + '-' + m + '-' + d);
  }

  handlePickupDateChange(event){
    var date = this.convertToDate(event._d);
    this.setState({ pickupDate : date   });
  }

  handleDeliveryDateChange(event){
    var date = this.convertToDate(event._d);
    this.setState({ deliveryDate : date   });
  }

  convertToTime(val){
    var h = val.getHours();
    var m = val.getMinutes();
    var s = val.getSeconds();
    return (h + ':' + m + ':' + s);
  }

  handleDeliveryTimeChange(event){
    var time = this.convertToTime(event._d);
    this.setState({ deliveryTime : time   })
  }

  handlePickupTimeChange(event){
    var time = this.convertToTime(event._d);
    this.setState({ pickupTime : time   });
  }

  handlePickupAddress(event){
    this.setState({ pickupAddress: event.target.value});
  }

  handleDeliveryAddress(event){
    this.setState({ deliveryAddress: event.target.value});
  }

  handleOrderComment(event){
    this.setState({
      orderComments : event.target.value,
    })
  }

  getPickUpDelivery(){
    if(this.state.serviceIds.length<=0){
      alert('Please select the services');
      return;
    }
    this.setState({
      showServices:false,
      showPickUpDelivery:true,
    })
    window.scrollTo(0,0);
  }

  goBackToCategory(){
    this.setState({
      selectedCategory : '',
      showBrands : false,
      showCategory : true,
    })
  }

  addToCart(){
    if(this.state.pickupDate === ''){
      alert('Please select Pickup Date');
      return;
    }
    else if(this.state.pickupTime === ''){
      alert('Please select Pickup Time');
      return;
    }
    else if(this.state.pickupAddress === ''){
      alert('Please select Pickup Address');
      return;
    }
    else if(this.state.deliveryDate === ''){
      alert('Please select Delivery Date');
      return;
    }
    else if(this.state.deliveryTime === ''){
      alert('Please select Delivery Time');
      return;
    }
    else if(this.state.deliveryAddress === ''){
      alert('Please select Delivery Address');
      return;
    }
    else{
      // fetch('http://softbizz.in/helmetLaundry/api/public/addToCart',{
      //       method: 'POST',
      //       headers: {
      //         'Accept': 'application/json',
      //         'Content-Type': 'application/json',
      //         'session-key': localStorage.getItem("key"),
      //         'user-id' : localStorage.getItem("user")
      //       },
      //       body: JSON.stringify({
      //           'order_type': "SERVICE",
      //           'category_id': this.state.selectedCategory.category_id,
      //           'brand_id': this.state.selectedBrand.brand_id,
      //           'service_ids': this.state.serviceIds,
      //           'preferred_pick_up_date': this.state.pickupDate,
      //           'preferred_pick_up_time': this.state.pickupTime,
      //           'pick_up_address': this.state.pickupAddress,
      //           'delivery_date': this.state.deliveryDate,
      //           'delivery_time': this.state.deliveryTime,
      //           'delivery_address': this.state.deliveryAddress,
      //           'pickup_delivery_id': this.state.pickupDeliveryServiceID,
      //           'order_comments': this.state.orderComments,
      //       })
      //       })
      //       .then(response => response.json())
      //       .then(json =>{
      //         if(json.error === true){
      //           if(json.sessionExpired === true){
      //             this.setState({
      //               openLoginModel:true,
      //             })
      //           }
      //           alert(json.message);
      //         }
      //         else{
      //           this.backToBookNewService();
      //           alert(json.message);
      //         }
      //       })
      //       .catch(error =>{
      //           console.log(error);
      // });
      this.setState({
        showPickUpDelivery:false,
        showCheckoutPage: true,
      })
    }
  }

  getPickUpDeliveryList(){
    fetch('http://softbizz.in/helmetLaundry/api/public/getPickupDeliveryServicesList',{
      method: 'GET',
      headers: {
        'session-key': localStorage.getItem("key"),
        'user-id' : localStorage.getItem("user")
      },
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          this.setState({
            openLoginModel:true,
          })
        }
        alert(json.message);
      }
      else{
        this.setState({
          pickUpDeliveryList :json.data,
          pickupDeliveryServiceID : json.data[0].pickup_delivery_id
        });
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  getServicesList(){
    fetch('http://softbizz.in/helmetLaundry/api/public/getServicesList',{
      method: 'GET',
      headers: {
        'session-key': localStorage.getItem("key"),
        'user-id' : localStorage.getItem("user")
      },
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          this.setState({
            openLoginModel:true,
          })
        }
        alert(json.message);
      }
      else{
        this.setState({
          servicesList :json.data
        });
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  getBrandList(){
    fetch('http://softbizz.in/helmetLaundry/api/public/getBrandList',{
      method: 'GET',
      headers: {
        'session-key': localStorage.getItem("key"),
        'user-id' : localStorage.getItem("user")
      },
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          this.setState({
            openLoginModel:true,
          })
        }
        alert(json.message);
      }
      else{
        this.setState({
          brandList :json.data
        });
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  getCategoryList(){
    fetch('http://softbizz.in/helmetLaundry/api/public/getCategoryList',{
      method: 'GET',
      headers: {
        'session-key': localStorage.getItem("key"),
        'user-id' : localStorage.getItem("user")
      },
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          this.setState({
            openLoginModel:true,
          })
        }
        alert(json.message);
      }
      else{
        this.setState({
          categoryList :json.data
        });
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  handleCoupen(event){
    this.setState({
      orderCoupen: event.target.value,
    })
  }

  checkoutData(){
    const { classes, ...rest } = this.props;
    var btn1 = {
      textAlign : 'center',
    }
    var root = {
      width: '100%',
      overflowX: 'auto',
    }
    var table = {
      minWidth: 700,
    }
    var total = 0;
    var pickupDeliveryService;
    for(var i=0; i<this.state.pickUpDeliveryList.length; i++){
      if(this.state.pickUpDeliveryList[i].pickup_delivery_id === this.state.pickupDeliveryServiceID){
        pickupDeliveryService = this.state.pickUpDeliveryList[i];
      }
    }
    if(this.state.showCheckoutPage){
      return(
        <GridContainer justify="center" spacing={24}>
                <GridItem lg={12}>
                  <h1>ORDER DETAILS</h1>
                </GridItem>
                <GridItem lg={12}>
                  <Card>
                    <CardBody> 
                      <Card>
                        <CardBody>  
                      <GridContainer justify='center' spacing={24}>
                        <GridItem lg={4}>
                          <h4><u>Details:</u></h4>
                          <p><b>Category:</b> {this.state.selectedCategory.category_name}</p>
                          <p><b>Brand:</b> {this.state.selectedBrand.brand_name}</p>
                        </GridItem>
                        <GridItem lg={4}>
                          <h4><u>Pickup Details</u></h4>
                          <p><b>Date:</b> {this.state.pickupDate}</p>
                          <p><b>Time:</b> {this.state.pickupTime}</p>
                          <p><b>Address:</b> {this.state.pickupAddress}</p>
                        </GridItem>
                        <GridItem lg={4}>
                          <h4><u>Delivery Details</u></h4>
                          <p><b>Date:</b> {this.state.deliveryDate}</p>
                          <p><b>Time:</b> {this.state.deliveryTime}</p>
                          <p><b>Address:</b> {this.state.deliveryAddress}</p>
                        </GridItem>
                      </GridContainer>
                      </CardBody>
                      </Card>
                        <GridItem lg={12}>
                        <Paper style={root}>
                          <Table style={table}>
                            <TableHead>
                              <TableRow>
                                <TableCell><h4><b>SERVICE</b></h4></TableCell>
                                <TableCell numeric><h4><b>PRICE</b></h4></TableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {this.state.servicesList.map(item => {
                                for(var i=0; i<this.state.serviceIds.length; i++){
                                  if(item.service_id === this.state.serviceIds[i]){
                                    total = total + parseInt(item.price);
                                    return(
                                      <TableRow key={item.service_id}>
                                        <TableCell component="th" scope="row">{item.service_name}</TableCell>
                                        <TableCell numeric>{item.price}</TableCell>
                                      </TableRow>
                                    )
                                  }
                                }
                              })}
                              <TableRow>
                                <TableCell component="th" scope="row"><b>Total</b></TableCell>
                                <TableCell numeric><b>{total}</b></TableCell>
                              </TableRow>
                              <TableRow>
                                <TableCell component="th" scope="row"><b>GST 18%</b></TableCell>
                                <TableCell numeric>+ {total * 0.18}</TableCell>
                              </TableRow>
                              <TableRow>
                                <TableCell component="th" scope="row"><b>Pickup/Delivery:</b> {pickupDeliveryService.pickup_delivery_name}</TableCell>
                                <TableCell numeric>+ {pickupDeliveryService.pickup_delivery_charge}</TableCell>
                              </TableRow>
                              <TableRow>
                                <TableCell component="th" scope="row"><b>Grand Total</b></TableCell>
                                <TableCell numeric><b>{parseInt(total) + parseInt(total * 0.18) + parseInt(pickupDeliveryService.pickup_delivery_charge)}</b></TableCell>
                              </TableRow>
                            </TableBody>
                          </Table>
                          </Paper>
                        </GridItem>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem lg={12} style={btn1}>
                  <Button color="primary">Proceed To Payment</Button>
                </GridItem>
        </GridContainer>
      )
    }
  }

  pickUpDeliveryData(){
    const { classes, ...rest } = this.props;
    var btn1 = {
      textAlign : 'end',
    }
    if(this.state.showPickUpDelivery){
      return(
        <GridContainer justify="center" spacing={24}>
                <GridItem lg={12}>
                  <h1>PICKUP / DELIVERY DETAILS</h1>
                </GridItem>
                <GridItem lg={6}>
                  <Card>
                    <CardBody>          
                      <h3>Enter Pickup Details</h3>
                      <GridContainer justify="center" spacing={24}>
                        <GridItem lg={6}>
                          <Datetime
                                inputProps={{ placeholder: "Select Pickup Date"}}
                                timeFormat={false}
                                closeOnSelect = {true}
                                onChange = {this.handlePickupDateChange.bind(this)}
                              />
                        </GridItem>
                        <GridItem lg={6}>
                          <Datetime
                                inputProps={{ placeholder: "Select Pickup Time" }}
                                dateFormat={false}
                                onChange = {this.handlePickupTimeChange.bind(this)}
                              />
                        </GridItem>
                      </GridContainer>
                      {/* <textarea cols='30' rows="10" placeholder='Enter Pickup Address'></textarea> */}
                      <CustomInput
                        labelText="Enter PickUp Address"
                        id="material"
                        formControlProps={{
                          onChange:this.handlePickupAddress.bind(this),
                          fullWidth: true
                        }}
                        inputProps={{
                          endAdornment: (<InputAdornment position="end"><Home/></InputAdornment>)
                        }}
                      />
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem lg={6}>
                  <Card>
                    <CardBody>
                      <h3>Enter Delivery Details</h3>
                      <GridContainer justify="center" spacing={24}>
                        <GridItem lg={6}>
                          <Datetime
                                inputProps={{ placeholder: "Select Delivery Date" }}
                                timeFormat={false}
                                closeOnSelect = {true}
                                onChange = {this.handleDeliveryDateChange.bind(this)}
                              />
                        </GridItem>
                        <GridItem lg={6}>
                          <Datetime
                                inputProps={{ placeholder: "Select Delivery Time" }}
                                dateFormat={false}
                                onChange = {this.handleDeliveryTimeChange.bind(this)}
                              />
                        </GridItem>
                      </GridContainer>
                      <CustomInput
                        labelText="Enter Delivery Address"
                        id="material"
                        formControlProps={{
                          onChange:this.handleDeliveryAddress.bind(this),
                          fullWidth: true
                        }}
                        inputProps={{
                          endAdornment: (<InputAdornment position="end"><Home/></InputAdornment>)
                        }}
                      />
                    </CardBody> 
                  </Card>
                </GridItem>
                
                          <GridItem lg={12}>
                            <Card>
                              <CardBody>
                                <GridContainer spacing={24}>
                                  <GridItem lg={12}>
                                    <div className={classes.root}>
                                      <FormControl component="fieldset" className={classes.formControl}>
                                        {/* <FormLabel component="legend">Select Pickup/Delivery Service</FormLabel> */}
                                        <h3>Select Pickup/Delivery Service</h3>
                                        <RadioGroup
                                          aria-label="Gender"
                                          name="gender1"
                                          className={classes.group}
                                          value={this.state.pickupDeliveryServiceID}
                                          onChange={this.handlePickupDeliveryService.bind(this)}
                                        >
                                        {this.state.pickUpDeliveryList.map(item => {
                                          if(item.flag === 1){
                                              return (
                                                  
                                                    <FormControlLabel key={item.pickup_delivery_id} value={item.pickup_delivery_id} control={<Radio />} label={item.pickup_delivery_name + ' || Charges: ' + item.pickup_delivery_charge + '/-'}/>
                                                  
                                        )}})}
                                        </RadioGroup>
                                      </FormControl>
                                    </div>
                                  </GridItem>
                                </GridContainer>
                              </CardBody>
                            </Card>
                          </GridItem>

                          <GridItem lg={12}>
                            <Card>
                              <CardBody>
                                <h3>Order Comments?</h3>
                                <CustomInput
                                  labelText="Enter Order Comments"
                                  id="material"
                                  formControlProps={{
                                    onChange:this.handleOrderComment.bind(this),
                                    fullWidth: true
                                  }}
                                  // inputProps={{
                                  //   endAdornment: (<InputAdornment position="end"><Home/></InputAdornment>)
                                  // }}
                                />
                              </CardBody> 
                            </Card>
                          </GridItem>
          
                           <GridItem xs={6} style={btn1}>
                             <Button color="primary" onClick={this.goBackToServices.bind(this)}>BACK</Button>
                           </GridItem>
                           <GridItem xs={6}>
                             <Button color="primary" onClick={this.addToCart.bind(this)}>Checkout</Button>
                           </GridItem>
              </GridContainer>
        )
    }
  }

  servicesListData(){
    const { classes, ...rest } = this.props;
    var btn1 = {
      textAlign : 'end',
    }
    if(this.state.showServices){
      return(
        <GridContainer justify="center" spacing={24}>
                <GridItem lg={12}>
                  <h1>SELECT SERVICES</h1>
                </GridItem>
                {this.state.servicesList.map(item => {
                  if(item.flag === 1 && item.category_id === this.state.selectedCategory.category_id){
                      return (
                          <GridItem lg={12} key={item.service_id}>
                            <Card>
                              <CardBody>
                                <GridContainer spacing={24}>
                                  <GridItem lg={1}>
                                    <FormControlLabel
                                      control={
                                        <Checkbox  onChange={this.selectService.bind(this)} value={item.service_id} />
                                      }
                                      label=""
                                    />
                                  </GridItem>
                                  <GridItem lg={4}>
                                    <img className={classes.imgCardTop} src={item.service_image} alt="Card-img-cap" />
                                  </GridItem>
                                  <GridItem lg={7}>
                                    <p><b>SERVICE:</b> {item.service_name}</p>
                                    <p><b>DESCRIPTION:</b> {item.service_description}</p>
                                    <p><b>PRICE:</b> {item.price} /-</p>
                                  </GridItem>
                                </GridContainer>
                              </CardBody>
                            </Card>
                          </GridItem>
                  )}})}
          
                           <GridItem xs={6} style={btn1}>
                             <Button color="primary" onClick={this.goBackToBrands.bind(this)}>BACK</Button>
                           </GridItem>
                           <GridItem xs={6}>
                             <Button color="primary" onClick={this.getPickUpDelivery.bind(this)}>NEXT</Button>
                           </GridItem>
              </GridContainer>
        )
    }
  }

  brandsListData(){
    const { classes, ...rest } = this.props;
    var btn1 = {
      textAlign : 'center',
    }
    if(this.state.showBrands){
      return(
      <GridContainer justify="center" spacing={24}>
              <GridItem lg={12}>
                <h1>SELECT BRAND</h1>
              </GridItem>
              {this.state.brandList.map(item => {
                if(item.flag === 1){
                    return (
                      <GridItem lg={3} onClick={this.getServices.bind(this, item)} key={item.brand_id}>
                        <Card>
                          <img className={classes.imgCardTop} src={item.brand_logo} alt="Card-img-cap" />
                          <CardBody>
                            <h4 className={classes.cardTitle}>{item.brand_name}</h4>
                          </CardBody>
                        </Card>
                      </GridItem>
                    )
                }
              })}
              <GridItem lg={12} style={btn1}>
                  <Button color="primary" onClick={this.goBackToCategory.bind(this)}>BACK</Button>
              </GridItem>
            </GridContainer>
      )
    }
  }

  categoryListData(){
    const { classes, ...rest } = this.props;
    if(this.state.showCategory){
      return(
      <GridContainer justify="center" spacing={24}>
              <GridItem lg={12}>
                <h1>SELECT CATEGORY</h1>
              </GridItem>
              {this.state.categoryList.map(item => {
                if(item.flag === 1){
                    return (
                      <GridItem lg={3} onClick={this.getBrands.bind(this, item)} key={item.category_id}>
                        <Card>
                          <img className={classes.imgCardTop} src={item.category_image} alt="Card-img-cap" />
                          <CardBody>
                            <h4 className={classes.cardTitle}>{item.category_name}</h4>
                          </CardBody>
                        </Card>
                      </GridItem>
                    )
                }
              })}
            </GridContainer>
      )
    }
  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          color="transparent"
          brand="Service" 
          rightLinks={<HeaderLinks openLoginModel= {this.state.openLoginModel}/>}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: "white"
          }}
          {...rest}
        />
        <Parallax small filter image={require("assets/img/profile-bg.jpg")} >
          <div className={classes.container}>
              <GridContainer>
                <GridItem>
                  <div className={classes.brand}>
                    <h1 className={classes.title}>Book Your Service</h1>
                    <h3 className={classes.subtitle}>
                      We Pick.. We Service.. We Deliver..
                    </h3>
                  </div>
                </GridItem>
              </GridContainer>
            </div>
        </Parallax>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div>
            <div className={classes.container}>
              {this.categoryListData()}
              {this.brandsListData()}
              {this.servicesListData()}
              {this.pickUpDeliveryData()}
              {this.checkoutData()}
              {/* <div className={classes.description}>
                <p>
                  An artist of considerable range, Chet Faker — the name taken
                  by Melbourne-raised, Brooklyn-based Nick Murphy — writes,
                  performs and records all of his own music, giving it a warm,
                  intimate feel with a solid groove structure.{" "}
                </p>
              </div> */}
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withStyles(servicePageStyle)(ServicePage);
