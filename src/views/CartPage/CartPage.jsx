import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
// Grid
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";

import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";

import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";

import servicePageStyle from "assets/jss/material-kit-react/views/servicePage.jsx";

class CartPage extends React.Component {
    constructor(props) {
      super(props);
      
      this.state = {
        openLoginModel: false,
      };
    }

    componentDidMount(){
      
    }

    cartData(){
      const { classes, ...rest } = this.props;
      if(this.state.showCategory){
        return(
        <GridContainer justify="center" spacing={24}>
                <GridItem lg={12}>
                  <h1>SELECT CATEGORY</h1>
                </GridItem>
                {this.state.categoryList.map(item => {
                  if(item.flag === '1'){
                      return (
                        <GridItem lg={3}>
                          <Card>
                            <CardBody>

                            </CardBody>
                          </Card>
                        </GridItem>
                      )
                  }
                })}
              </GridContainer>
        )
      }
    }  

    render() {
      const { classes, ...rest } = this.props;
      return (
        <div>
          <Header
            color="transparent"
            brand="Service" 
            rightLinks={<HeaderLinks openLoginModel= {this.state.openLoginModel}/>}
            fixed
            changeColorOnScroll={{
              height: 200,
              color: "white"
            }}
            {...rest}
          />
          <Parallax small filter image={require("assets/img/profile-bg.jpg")} >
            <div className={classes.container}>
                <GridContainer>
                  <GridItem>
                    <div className={classes.brand}>
                      <h1 className={classes.title}>CART</h1>
                      {/* <h3 className={classes.subtitle}>
                        We Pick.. We Service.. We Deliver..
                      </h3> */}
                    </div>
                  </GridItem>
                </GridContainer>
              </div>
          </Parallax>
          <div className={classNames(classes.main, classes.mainRaised)}>
            <div>
              <div className={classes.container}>
                {this.cartData()}
                {/* {this.brandsListData()}
                {this.servicesListData()}
                {this.pickUpDeliveryData()} */}
                {/* <div className={classes.description}>
                  <p>
                    An artist of considerable range, Chet Faker — the name taken
                    by Melbourne-raised, Brooklyn-based Nick Murphy — writes,
                    performs and records all of his own music, giving it a warm,
                    intimate feel with a solid groove structure.{" "}
                  </p>
                </div> */}
              </div>
            </div>
          </div>
          <Footer />
        </div>
      );
    }
}

export default withStyles(servicePageStyle)(CartPage);