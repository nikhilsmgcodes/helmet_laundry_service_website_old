import Components from "views/Components/Components.jsx";
import LandingPage from "views/LandingPage/LandingPage.jsx";
import ProfilePage from "views/ProfilePage/ProfilePage.jsx";
import LoginPage from "views/LoginPage/LoginPage.jsx";
import ServicePage from "views/ServicePage/ServicePage.jsx";
import CartPage from "views/CartPage/CartPage.jsx";

var indexRoutes = [
  { path: "/landing-page", name: "LandingPage", component: LandingPage },
  { path: "/profile-page", name: "ProfilePage", component: ProfilePage },
  { path: "/login-page", name: "LoginPage", component: LoginPage },
  { path: "/service", name: "ServicePage", component: ServicePage },
  { path: "/cart", name: "CartPage", component: CartPage },
  { path: "/", name: "Components", component: Components }
];

export default indexRoutes;
