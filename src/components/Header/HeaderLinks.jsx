/*eslint-disable*/
import React from "react";
// react components for routing our app without refresh
import { Link } from "react-router-dom";
// nodejs library to set properties for components
import PropTypes from "prop-types";

import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Phone from "@material-ui/icons/Phone";
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/Person";
import Dialog from "@material-ui/core/Dialog";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Grid from '@material-ui/core/Grid';

// @material-ui/icons
import { Apps, CloudDownload, Build, ShoppingCart, ExitToApp, AccountCircle } from "@material-ui/icons";

// core components 
import CustomDropdown from "components/CustomDropdown/CustomDropdown.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Modal from '@material-ui/core/Modal';
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";

import headerLinksStyle from "assets/jss/material-kit-react/components/headerLinksStyle.jsx";
import loginPageStyle from "assets/jss/material-kit-react/views/loginPage.jsx";

class HeaderLinks extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      openLoginModel:false,
      openSignUpModel:false,
      verifyOtpPage:false,
      getOtpPage:true,
      signUpPage:false,
      openResetModel:false,

      getResetOtpPage:true,
      verifyResetOtpPage:false,
      resetPasswordPage:false,

      showLoginSignupButton: true,
      userPhone : '',
      otp : '',
      userName : '',
      userEmail : '',
      userPassword : ''
    };
  }

  componentWillMount(){
    this.checkSessionData();
  }

  checkSessionData(){
        fetch('http://softbizz.in/helmetLaundry/api/public/checkSessionData',{
          method: 'GET',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'user-id': localStorage.getItem('user'),
              'session-key' : localStorage.getItem('key')
          },
          })
          .then(response => response.json())
          .then(json =>{
            if(json.error === true){
              if(json.sessionExpired === true){
                this.setState({
                  openLoginModel:true,
                })
              }
              alert(json.message);
            }
            else{
              this.setState({
                openLoginModel :false,
                showLoginSignupButton: false,
              });
            }
          })
          .catch(error =>{
              console.log(error);
    });
  }

  componentWillReceiveProps(){
    console.log('componentWillReceiveProps ' +this.props.openLoginModel);
    this.setState({openLoginModel:this.props.openLoginModel});
  }

  // shouldComponentUpdate(){
  //   console.log('componentWillReceiveProps ' +this.props.openLoginModel);
  //   this.setState({openLoginModel:this.props.openLoginModel});
  // }

  // componentWillReceiveProps(){
  //   console.log('componentWillReceiveProps'+this.props.openLoginModel);
  //   this.setState({openLoginModel:this.props.openLoginModel});
  // }

  handleClose(){
    this.setState({
      openLoginModel:false,
      openSignUpModel:false,
      verifyOtpPage:false,
      getOtpPage:true,

      openResetModel:false,
      verifyResetOtpPage:false,
      getResetOtpPage:true,

      userPhone : '',
      otp : '',
      userName : '',
      userEmail : '',
      userPassword : ''
    })
  }

  loginModel(){
    console.log('login Model');
    this.setState({
      openSignUpModel:false,
      openLoginModel:true,
    });
  }

  signUpModel(){
    console.log('signup model');
    this.setState({
      openLoginModel:false,
      openSignUpModel:true,
    });
  }

  getOtp(){
    if(this.state.userPhone.length < 10){
      alert('Please Enter 10 Digit Phone Number');
      return;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/getOtp',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'phone': this.state.userPhone,
                'status' : 'REGISTRATION'
            })
            })
            .then(response => response.json())
            .then(json =>{
                if(json.error === true){
                    alert(json.message);
                }
                else{
                    this.setState({
                      getOtpPage:false,
                      verifyOtpPage:true,
                    })
                    alert(json.message);
                }
            })
            .catch(error =>{
                console.log(error);
    });
  }

  verifyOtp(){
    if(this.state.otp.length <= 0){
      alert('Please Enter OTP');
      return;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/verifyOtp',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'phone': this.state.userPhone,
                'otp' : this.state.otp
            })
            })
            .then(response => response.json())
            .then(json =>{
                if(json.error === true){
                    alert(json.message);
                }
                else{
                    this.setState({
                      verifyOtpPage:false,
                      signUpPage:true,
                    })
                    alert(json.message);
                }
            })
            .catch(error =>{
                console.log(error);
    });
  }

  getResetOtp(){
    if(this.state.userPhone.length < 10){
      alert('Please Enter 10 Digit Phone Number');
      return;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/getOtp',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'phone': this.state.userPhone,
                'status' : 'FORGOTPASSWORD'
            })
            })
            .then(response => response.json())
            .then(json =>{
                if(json.error === true){
                    alert(json.message);
                }
                else{
                    this.setState({
                      getResetOtpPage:false,
                      verifyResetOtpPage:true,
                    })
                    alert(json.message);
                }
            })
            .catch(error =>{
                console.log(error);
    });
  }

  verifyResetOtp(){
    if(this.state.otp.length <= 0){
      alert('Please Enter OTP');
      return;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/verifyOtp',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'phone': this.state.userPhone,
                'otp' : this.state.otp
            })
            })
            .then(response => response.json())
            .then(json =>{
                if(json.error === true){
                    alert(json.message);
                }
                else{
                    this.setState({
                      verifyResetOtpPage:false,
                      resetPasswordPage:true,
                    })
                    alert(json.message);
                }
            })
            .catch(error =>{
                console.log(error);
    });
  }

  singUp(){
    if(this.state.userName === '' && this.state.userEmail === '' && this.state.userPassword === ''){
      alert('Please Fill all details');
      return;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/userRegistration',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'phone': this.state.userPhone,
                'name': this.state.userName,
                'email': this.state.userEmail,
                'password' : this.state.userPassword
            })
            })
            .then(response => response.json())
            .then(json =>{
                if(json.error === true){
                    alert(json.message);
                }
                else{
                    localStorage.setItem('user',json.data.user_id);
                    localStorage.setItem('key',json.session_key);
                    this.setState({
                      userData: json.data,
                      showLoginSignupButton : false,
                      signUpPage:false,
                      getOtpPage:true,
                      openSignUpModel:false,
                      openLoginModel:false,
                      userPhone: '',
                      userName:'',
                      userEmail:'',
                      userPassword:''
                    })
                    alert(json.message);
                }
            })
            .catch(error =>{
                console.log(error);
    });
  }

  resetPassword(){
    if(this.state.userPassword.length <= 0){
      alert('Please Enter New Password');
      return;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/resetForgotPassword',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'phone': this.state.userPhone,
                'password' : this.state.userPassword
            })
            })
            .then(response => response.json())
            .then(json =>{
                if(json.error === true){
                    alert(json.message);
                }
                else{
                    this.setState({
                      openSignUpModel:false,
                      verifyOtpPage:false,
                      getOtpPage:true,

                      openResetModel:false,
                      verifyResetOtpPage:false,
                      getResetOtpPage:true,

                      openLoginModel:true,
                      userPhone: '',
                      otp: '',
                      userName:'',
                      userEmail:'',
                      userPassword:'',
                    })
                    alert(json.message);
                }
            })
            .catch(error =>{
                console.log(error);
    });
  }

  login(){
    if(this.state.userPhone.length < 10){
      alert('Please Enter 10 Digit Phone Number');
      return;
    }
    if(this.state.userPassword.length <= 0){
      alert('Please Enter password');
      return;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/userLogin',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'login_type': 1,
                'phone': this.state.userPhone,
                'password' : this.state.userPassword
            })
            })
            .then(response => response.json())
            .then(json =>{
                if(json.error === true){
                    alert(json.message);
                }
                else{
                    localStorage.setItem('user',json.data.user_id);
                    localStorage.setItem('key',json.session_key);
                    this.setState({
                      userData: json.data,
                      showLoginSignupButton : false,
                      signUpPage:false,
                      getOtpPage:true,
                      openSignUpModel:false,
                      openLoginModel:false,
                      userPhone: '',
                      userName:'',
                      userEmail:'',
                      userPassword:''
                    })
                    alert(json.message);
                }
            })
            .catch(error =>{
                console.log(error);
    });
  }

  handleUserPhone(event){
    console.log(event.target.value);
    this.setState({
      userPhone : event.target.value,
    })
  }

  handleOtp(event){
    this.setState({
      otp : event.target.value,
    })
  }

  handleUserName(event){
    this.setState({
      userName : event.target.value,
    })
  }

  handleUserEmail(event){
    this.setState({
      userEmail : event.target.value,
    })
  }

  handleUserPassword(event){
    this.setState({
      userPassword : event.target.value,
    })
  }

  getOtpPageData(){
    const {classes} = this.props;
    var btnStyle = {
      textAlign : 'center'
    }
    var btnStyle1 = {
      width: '100%'
    }
    if(this.state.getOtpPage){
      return(
        <div>
          <CustomInput
            labelText="Phone..."
            id="phone"
            formControlProps={{
              onChange:this.handleUserPhone.bind(this),
              fullWidth: true
            }}
            inputProps={{
              type: "phone",
              value: this.state.userPhone,
              endAdornment: (
                <InputAdornment position="end">
                  <Phone className={classes.inputIconsColor} />
                </InputAdornment>
              ),
            }}
          />
          <Button color="primary" size="sm" style={btnStyle1} onClick={this.getOtp.bind(this)}>Get OTP</Button>
        </div>
      )
    }
    else if(this.state.verifyOtpPage){
      return(
        <div>
                      <CustomInput
                        labelText="OTP..."
                        id="otp"
                        formControlProps={{
                          onChange:this.handleOtp.bind(this),
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "otp",
                          value: this.state.otp,
                          endAdornment: (
                            <InputAdornment position="end">
                              <Icon className={classes.inputIconsColor}>
                                lock_outline
                              </Icon>
                            </InputAdornment>
                          )
                        }}
                      /> 
          <Button color="primary" size="sm" style={btnStyle1} onClick={this.verifyOtp.bind(this)}>Verify OTP</Button>
        </div>
      )
    }
    else if(this.state.signUpPage){
      return(
        <div>
                    <CustomInput
                        labelText="Name..."
                        id="people"
                        formControlProps={{
                          onChange:this.handleUserName.bind(this),
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "people",
                          value: this.state.userName,
                          endAdornment: (
                            <InputAdornment position="end">
                              <People className={classes.inputIconsColor} />
                            </InputAdornment>
                          )
                        }}
                      />
                      <CustomInput
                        labelText="Email..."
                        id="email"
                        formControlProps={{
                          onChange:this.handleUserEmail.bind(this),
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "email",
                          value: this.state.userEmail,
                          endAdornment: (
                            <InputAdornment position="end">
                              <Email className={classes.inputIconsColor} />
                            </InputAdornment>
                          )
                        }}
                      />
                      <CustomInput
                        labelText="Password..."
                        id="pass"
                        formControlProps={{
                          onChange:this.handleUserPassword.bind(this),
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "password",
                          value: this.state.userPassword,
                          endAdornment: (
                            <InputAdornment position="end">
                              <Icon className={classes.inputIconsColor}>
                                lock_outline
                              </Icon>
                            </InputAdornment>
                          )
                        }}
                      />  
                      <Button color="primary" size="sm" style={btnStyle1} onClick={this.singUp.bind(this)}>SignUp</Button>       
        </div> 
      )
    }
  }

  getOtpResetPageData(){
    const {classes} = this.props;
    var btnStyle = {
      textAlign : 'center'
    }
    var btnStyle1 = {
      width: '100%'
    }
    if(this.state.getResetOtpPage){
      return(
        <div>
          <CustomInput
            labelText="Phone..."
            id="phone"
            formControlProps={{
              onChange:this.handleUserPhone.bind(this),
              fullWidth: true
            }}
            inputProps={{
              type: "phone",
              value: this.state.userPhone,
              endAdornment: (
                <InputAdornment position="end">
                  <Phone className={classes.inputIconsColor} />
                </InputAdornment>
              ),
            }}
          />
          <Button color="primary" size="sm" style={btnStyle1} onClick={this.getResetOtp.bind(this)}>Get OTP</Button>
        </div>
      )
    }
    else if(this.state.verifyResetOtpPage){
      return(
        <div>
                      <CustomInput
                        labelText="OTP..."
                        id="otp"
                        formControlProps={{
                          onChange:this.handleOtp.bind(this),
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "otp",
                          value: this.state.otp,
                          endAdornment: (
                            <InputAdornment position="end">
                              <Icon className={classes.inputIconsColor}>
                                lock_outline
                              </Icon>
                            </InputAdornment>
                          )
                        }}
                      /> 
          <Button color="primary" size="sm" style={btnStyle1} onClick={this.verifyResetOtp.bind(this)}>Verify OTP</Button>
        </div>
      )
    }
    else if(this.state.resetPasswordPage){
      return(
        <div>
                      <CustomInput
                        labelText="Password..."
                        id="pass"
                        formControlProps={{
                          onChange:this.handleUserPassword.bind(this),
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "password",
                          value: this.state.userPassword,
                          endAdornment: (
                            <InputAdornment position="end">
                              <Icon className={classes.inputIconsColor}>
                                lock_outline
                              </Icon>
                            </InputAdornment>
                          )
                        }}
                      />  
                      <Button color="primary" size="sm" style={btnStyle1} onClick={this.resetPassword.bind(this)}>Reset Password</Button>       
        </div> 
      )
    }
  }

  resetModel(){
    this.setState({
      openLoginModel:false,
      openSignUpModel:false,
      openResetModel:true,
    })
  }

  resetModelData() {
    const {classes} = this.props;
    var btnStyle = {
      textAlign : 'center'
    }
    var btnStyle1 = {
      width: '100%'
    }
    var gridStyle = {
      width: '108%'
    }
    return(
        <Modal open={this.state.openResetModel} onClose={this.handleClose.bind(this)}>
          <div className={classes.container}>
            <GridContainer justify="center" >
              <GridItem xs={12} sm={12} md={4}>
                <Card>
                  <form className={classes.form}>
                    <CardHeader color="primary" className={classes.cardHeader}>
                      <h4>Reset Password</h4>
                    </CardHeader>
                    <CardBody>
                      {this.getOtpResetPageData()}
                    </CardBody>
                    <CardFooter className={classes.cardFooter}>
                      <GridContainer style={gridStyle} spacing={24}>
                        <GridItem xs={6} style={btnStyle}>
                          <Button color="primary" size="sm" style={btnStyle1} onClick={this.loginModel.bind(this)}>Login</Button>
                        </GridItem>
                        <GridItem xs={6} style={btnStyle}>
                          <Button color="primary" size="sm" style={btnStyle1} onClick={this.handleClose.bind(this)}>Cancel</Button>
                        </GridItem>
                      </GridContainer>
                    </CardFooter>
                  </form>
                </Card>
              </GridItem>
            </GridContainer>
            </div>
          </Modal>
    )
  }

  SignUpModelData() {
    const {classes} = this.props;
    var btnStyle = {
      textAlign : 'center'
    }
    var btnStyle1 = {
      width: '100%'
    }
    var gridStyle = {
      width: '108%'
    }
    return(
        <Modal open={this.state.openSignUpModel} onClose={this.handleClose.bind(this)}>
          <div className={classes.container}>
            <GridContainer justify="center" >
              <GridItem xs={12} sm={12} md={4}>
                <Card>
                  <form className={classes.form}>
                    <CardHeader color="primary" className={classes.cardHeader}>
                      <h4>SignUp</h4>
                    </CardHeader>
                    <CardBody>
                      {this.getOtpPageData()}
                    </CardBody>
                    <CardFooter className={classes.cardFooter}>
                      <GridContainer style={gridStyle} spacing={24}>
                        <GridItem xs={6} style={btnStyle}>
                          <Button color="primary" size="sm" style={btnStyle1} onClick={this.loginModel.bind(this)}>Login</Button>
                        </GridItem>
                        <GridItem xs={6} style={btnStyle}>
                          <Button color="primary" size="sm" style={btnStyle1} onClick={this.handleClose.bind(this)}>Cancel</Button>
                        </GridItem>
                      </GridContainer>
                    </CardFooter>
                  </form>
                </Card>
              </GridItem>
            </GridContainer>
            </div>
          </Modal>
    )
  }


  loginModelData() {
    const {classes} = this.props;
    var btnStyle = {
      textAlign : 'center'
    }
    var btnStyle1 = {
      width: '100%'
    }
    return(
        <Modal open={this.state.openLoginModel} onClose={this.handleClose.bind(this)}>
          <div className={classes.container}>
            <GridContainer justify="center" >
              <GridItem xs={12} sm={12} md={4}>
                <Card>
                  <form className={classes.form}>
                    <CardHeader color="primary" className={classes.cardHeader}>
                      <h4>Login</h4>
                      <div className={classes.socialLine}>
                        <Button
                          justIcon
                          href="#pablo"
                          target="_blank"
                          color="transparent"
                          onClick={e => e.preventDefault()}
                        >
                          <i className={"fab fa-twitter"} />
                        </Button>
                        <Button
                          justIcon
                          href="#pablo"
                          target="_blank"
                          color="transparent"
                          onClick={e => e.preventDefault()}
                        >
                          <i className={"fab fa-facebook"} />
                        </Button>
                        <Button
                          justIcon
                          href="#pablo"
                          target="_blank"
                          color="transparent"
                          onClick={e => e.preventDefault()}
                        >
                          <i className={"fab fa-google-plus-g"} />
                        </Button>
                      </div>
                    </CardHeader>
                    <p className={classes.divider}>Or Be Classical</p>
                    <CardBody>
                      <CustomInput
                        labelText="Phone..."
                        id="phone"
                        formControlProps={{
                          onChange:this.handleUserPhone.bind(this),
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "phone",
                          value: this.state.userPhone,
                          endAdornment: (
                            <InputAdornment position="end">
                              <Phone className={classes.inputIconsColor} />
                            </InputAdornment>
                          )
                        }}
                      />
                      <CustomInput
                        labelText="Password"
                        id="pass"
                        formControlProps={{
                          onChange:this.handleUserPassword.bind(this),
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "password",
                          value: this.state.userPassword,
                          endAdornment: (
                            <InputAdornment position="end">
                              <Icon className={classes.inputIconsColor}>
                                lock_outline
                              </Icon>
                            </InputAdornment>
                          )
                        }}
                      />
                    </CardBody>
                    <CardFooter className={classes.cardFooter}>
                    <GridContainer justify="center" >
                      <GridItem xs={12} style={btnStyle}>
                        <Button color="primary" size="sm" style={btnStyle1} onClick={this.login.bind(this)}>Login</Button>
                      </GridItem>
                      <GridItem xs={12} style={btnStyle}>
                        <Button color="primary" simple size="sm" style={btnStyle1} onClick={this.resetModel.bind(this)}>Forgot Password?</Button>
                      </GridItem>
                      <GridItem xs={12} sm={6} style={btnStyle}>
                        <Button color="primary" size="sm" style={btnStyle1} onClick={this.signUpModel.bind(this)}>SignUp</Button>
                      </GridItem>
                      <GridItem xs={12} sm={6} style={btnStyle}>
                        <Button color="primary" size="sm" style={btnStyle1} onClick={this.handleClose.bind(this)}>Cancel</Button>
                      </GridItem>
                    </GridContainer>
                    </CardFooter>
                  </form>
                </Card>
              </GridItem>
            </GridContainer>
            </div>
          </Modal>
    )
  }

  handleLogout(){
    var con = window.confirm('Sure Want to Logout?');
    if(con){
      localStorage.clear();
      this.setState({
        showLoginSignupButton : true,
      });                       
    }
  }

  loginLogoutData(){
    const { classes } = this.props;
    if(this.state.showLoginSignupButton){
      return(
        
          <Button
            color="transparent"
            className={classes.button}
            onClick={this.loginModel.bind(this)}
          ><AccountCircle/> Login/Signup
          </Button>
  
      )
    }
    else{
      return(
       
          <Button
            color="transparent"
            className={classes.button}
            onClick={this.handleLogout.bind(this)}
          ><ExitToApp /> Logout
          </Button>

      )
    }
  }

  cartData(){
    const { classes } = this.props;
    if(!this.state.showLoginSignupButton){
      return(
          <Link to="/cart" className={classes.dropdownLink} className={classes.navLink}>
               <ShoppingCart className={classes.icons} /> Cart
            </Link>
      )
    }
    else{
      return(
        null
      )
    }
  }

  render(){
    const { classes } = this.props;
    return (
      <div>
      <List className={classes.list}>
        {/* <ListItem className={classes.listItem}>
          <CustomDropdown
            noLiPadding
            buttonText="Components"
            buttonProps={{
              className: classes.navLink,
              color: "transparent"
            }}
            buttonIcon={Apps}
            dropdownList={[
              <Link to="/" className={classes.dropdownLink}>
                All components
              </Link>,
              <a
                href="https://creativetimofficial.github.io/material-kit-react/#/documentation"
                target="_blank"
                className={classes.dropdownLink}
              >
                Documentation
              </a>
            ]}
          />
        </ListItem> */}
        {/* <ListItem className={classes.listItem}>
          <Button
            href="https://www.creative-tim.com/product/material-kit-react"
            color="transparent"
            target="_blank"
            className={classes.navLink}
          >
            <CloudDownload className={classes.icons} /> Download
          </Button>
        </ListItem> */}
        <ListItem className={classes.listItem}>
            <Link to="/service" className={classes.dropdownLink} className={classes.navLink}>
               <Build className={classes.icons} /> Book Service
            </Link>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Tooltip
            id="instagram-twitter"
            title="Follow us on twitter"
            placement={window.innerWidth > 959 ? "top" : "left"}
            classes={{ tooltip: classes.tooltip }}
          >
            <Button
              href="https://twitter.com/ideesys"
              target="_blank"
              color="transparent"
              className={classes.navLink}
            >
              <i className={classes.socialIcons + " fab fa-twitter"} />
            </Button>
          </Tooltip>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Tooltip
            id="instagram-facebook"
            title="Follow us on facebook"
            placement={window.innerWidth > 959 ? "top" : "left"}
            classes={{ tooltip: classes.tooltip }}
          >
            <Button
              color="transparent"
              href="https://www.facebook.com/ideesys"
              target="_blank"
              className={classes.navLink}
            >
              <i className={classes.socialIcons + " fab fa-facebook"} />
            </Button>
          </Tooltip>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Tooltip
            id="instagram-tooltip"
            title="Follow us on instagram"
            placement={window.innerWidth > 959 ? "top" : "left"}
            classes={{ tooltip: classes.tooltip }}
          >
            <Button
              color="transparent"
              href="https://www.instagram.com/ideesys" 
              target="_blank"
              className={classes.navLink}
            >
              <i className={classes.socialIcons + " fab fa-instagram"} />
            </Button>
          </Tooltip>
        </ListItem>
        <ListItem className={classes.listItem}>
          {this.loginLogoutData()}
        </ListItem>
        <ListItem className={classes.listItem}>
          {this.cartData()}
        </ListItem>
      </List>
        {this.loginModelData()}
        {this.SignUpModelData()}
        {this.resetModelData()}
      </div>
    );
  }
}
export default withStyles(headerLinksStyle)(HeaderLinks);
